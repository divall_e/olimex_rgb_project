# Olimex RGB LED controller with OTA update

## Libraries 
EPICS 3.14, OTA update tested on ESP32 lib 2.0.14

```
#include <ETH.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
```

## Ethernet connection setup

```
void setup() {
  // Setup event listener
  WiFi.onEvent(WiFiEventOlimex);
  
  // Setip hostname .. might fail
  WiFi.setHostname(hostname.c_str());  

  // Start Ethernet
  ETH.begin();
}

void loop() {
    // Handle Arduino OTA
    ArduinoOTA.handle();
    
    // Receive
    ReadUDPPacket();
    
    // Send
    SendUDP();
    streamUDP();
}
```
In event handler use ```ETH``` naming (e.g. ```case ARDUINO_EVENT_ETH_START:```)
```
void WiFiEventOlimex(WiFiEvent_t event)
{
  switch (event) {
    case <CASE>:
  }
}
```

## Arduino OTA update

```
void setup() {
  // Start OTA
  ArduinoOTA.begin();
}

void loop() {
    // Handle Arduino OTA
    ArduinoOTA.handle();
}
```

#### Network
##### camtest PC on machine network

```
ssh <username>@sf-gw
ssh sf-lc7
ssh camtest
arduino &
```

##### Laptop on endstation network

- [ ] Use external dongle and change **Bypass Role** in Packet Fence to <endstation network>
- [ ] Re-plug dongle
- [ ] Start Arduino IDE (1.8 or 2.X)

#### Restart
Send ```:X``` to the device.
```
echo ":X" > /dev/udp/<DEVICE-IP>/44444
```

#### Links for HttpUpdate and server based updates

HttpUpdate

https://github.com/suculent/esp32-http-update/blob/master/examples/httpUpdate/httpUpdate.ino

https://github.com/espressif/arduino-esp32/tree/master/libraries/Update/examples

AutoConnect with python server

https://hieromon.github.io/AutoConnect/otaserver.html

https://forum.arduino.cc/t/webserver-ota-client-print-server-send/693298/7

https://github.com/sglahn/ota-server

https://microchip.my.site.com/s/article/How-to-run-a-local-web-server-using-Python-to-update-firmware-OTA

