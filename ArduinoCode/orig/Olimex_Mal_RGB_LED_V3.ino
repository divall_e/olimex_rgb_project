//adapt quad counter code used with picomotors to control LED chains

//https://adafruit.github.io/Adafruit_NeoPixel/html/class_adafruit___neo_pixel.html
//https://funduino.de/nr-17-ws2812-neopixel

String hostname = "SL-LRGB1";


#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif
#define PIN 16 // Hier wird angegeben, an welchem digitalen Pin die WS2812 LEDs bzw. NeoPixel angeschlossen sind
#define NUMPIXELS 300 // Hier wird die Anzahl der angeschlossenen WS2812 LEDs bzw. NeoPixel angegeben
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);



float LPeriod =  30;    // P     >Period in LEDs                            
float LNum    = 200;    // N     >Number to calculate
byte  LCol1   =   4;    // c     >First colour (pos in array)
byte  LCol2   =  13;    // C     >2nd colour
byte  LOn     =   1;    // G
float LStep   =  0.2;   // S     >Seconds per change
float LDist   =    1;   // D     >Distance per change       -->added to LPos
float LBDist  =    1;   // B     >Beat Distance per change  -->added to LBeat  
byte  LMode   =    0;   // M     >0=Static  1=move
int32_t LTimer =  60;   // T     >seconds till off  (LOn set to 0) - compared to LCtr
int LBr = 32;           // I     >LED intensity/brightness 0..255

float LPos;             //       >Phase of the sine wave in LEDs
float LBeat;            //       >fading of one colour into another in degrees?
float LCtr;             //       >Time counting up in ~Seconds  
int stepper = 0;
int colord = 0;

const byte pdcs[54] = {     //Pre-Defined-Colours
    0,   0,   0,  //0 Black
  170,   0,   0,  //1  dark red 
  255,   0,   0,  //2  red
  255, 170,   0,  //3  orange
  255, 255,   0,  //4  yellow
  85 , 255, 127,  //5  bright green 
    0, 170,   0,  //6  dark green
    0,  85,   0,  //7  darker green
    0, 170, 127,  //8  dirty green
    0,  85, 127,  //9  dirty blue
    0, 170, 255,  //10  sky blue
  170, 255, 255,  //11 light blue
    0,   0, 255,  //12 royal blue
    0,   0, 127,  //13 dark blue
  255,   0, 255,  //14 pink
  170,   0, 255,  //15 purple
  170,   0, 127,  //16 red/purple
  200, 200, 200 };//17 White 

const byte pdcsSize = 54;

byte Colours[150];  //Copy above predifed colour









uint32_t  SamplesBuffer[2048];
uint32_t  SamplesBufferIndex;
uint32_t  BufferFull;   //0=2nd half full  1024=first half, else 1= not ready
int32_t    encBuf[2];
const word BufferSendSize  =256;  //power of 2=>128*4bytes*2channels=1024 bytes in packet....
const word BufferSendSizeM1=255;

hw_timer_t *Read_timer = NULL;

//Ethernet stuff
#define ETH_CLK_MODE ETH_CLOCK_GPIO17_OUT
#define ETH_PHY_POWER 12

#include <ETH.h>           //ETH.h uses the RJ45 rather than the actual wifi
#include <WiFiUdp.h>
static bool eth_connected = false;
char packetBuffer[255]; //buffer to hold incoming packet
uint8_t  ReplyBuffer[1200] = "acknowledged";       // a string to send back
uint8_t  ReplyBuffer2[1000] = "acknowledged"; 


int packetSize;
byte UDPSend=0;
  WiFiUDP Udp;

IPAddress IOCIP,IOCIP2;
unsigned int IOCPort = 0;

//General
byte DebugLevel;

void IRAM_ATTR onReadTimer(){     //Sample timer - store position when ticks
/*  SamplesBuffer[SamplesBufferIndex]=QCount1.count;
  if ((SamplesBufferIndex & BufferSendSizeM1)==BufferSendSizeM1){//last buffer at 0 or 128, 256, 384...
    BufferFull=SamplesBufferIndex & (65535-BufferSendSizeM1);
  }
  SamplesBufferIndex=(SamplesBufferIndex+1) & 2047;
*/
 // encBuf[0]=QCount1.count;  //just save 1 value and set the flag
 // encBuf[1]=QCount2.count;
  BufferFull=1;
  
}


//Enc 1
/*void IRAM_ATTR QC1A() {
  if ( digitalRead(QCount1.PINA) ==  digitalRead(QCount1.PINB)) {
    QCount1.count ++;
  } else {
    QCount1.count --;
  }
}*/



void WiFiEvent(WiFiEvent_t event)
{
  Serial.println("ETH Event....");
  switch (event) {
    case SYSTEM_EVENT_ETH_START:
      Serial.println("ETH Started");
      //set eth hostname here
      ETH.setHostname(hostname.c_str());
      break;
    case SYSTEM_EVENT_ETH_CONNECTED:
      Serial.println("ETH Connected");
      break;
    case SYSTEM_EVENT_ETH_GOT_IP:
      Serial.print("ETH MAC: ");
      Serial.print(ETH.macAddress());
      Serial.print(", IPv4: ");
      Serial.print(ETH.localIP());
      //DispScrollString (ETH.localIP());// is an array so need to be cleverer
      if (ETH.fullDuplex()) {
        Serial.print(", FULL_DUPLEX");
      }
      Serial.print(", ");
      Serial.print(ETH.linkSpeed());
      Serial.println("Mbps");
      eth_connected = true;
      Udp.begin(44444);
      break;
    case SYSTEM_EVENT_ETH_DISCONNECTED:
      Serial.println("ETH Disconnected");
      eth_connected = false;
      break;
    case SYSTEM_EVENT_ETH_STOP:
      Serial.println("ETH Stopped");
      eth_connected = false;
      break;
    default:
      break;
  }
}


void CalcColoursSolid(byte c){
int i;
//byte c;
  c=c*3;
  for (i=0;i<LNum;i+=1){   pixels.setPixelColor(i,pixels.Color(Colours[c],Colours[c+1],Colours[c+2]));   }//for loop  
}


void CalcColoursSine(float pos, float power, float beatPhase){  //pos = pattern start in LEDs
int i;
float sine,asine,sbp,cbp;
float r1,r2,g1,g2,b1,b2,PiPer;
  sbp=sin(beatPhase*2*3.1415/360);//beat in degrees
  r1=Colours[LCol1*3];g1=Colours[LCol1*3+1];b1=Colours[LCol1*3+2];
  r2=Colours[LCol2*3];g2=Colours[LCol2*3+1];b2=Colours[LCol2*3+2];
  PiPer=2*3.1415/LPeriod;
  
  for (i=0;i<LNum;i+=1){
    sine = 0.5+sbp*sin((pos+i)*PiPer);  //proportion of col1
    sine= pow(sine , power);            //power gives smaller peaks
    asine= 1-sine;                             //proportion of col2

    pixels.setPixelColor(i,pixels.Color(r1*sine+r2*asine,g1*sine+g2*asine,b1*sine+b2*asine));

    
  }//for loop
  
}



void setup() {
byte i;
  Serial.begin(115200);

  for (i=0;i<pdcsSize;i+=1){Colours[i]=pdcs[i];}   //Load Colours Array

  
  //pinMode(QCount1.PINA, INPUT);pinMode(QCount1.PINB, INPUT);
  //attachInterrupt(QCount1.PINA, QC1A, CHANGE);
  //attachInterrupt(QCount1.PINB, QC1B, CHANGE);

  
  Read_timer = timerBegin(0, 8000, true); // 100uS       //timer number [0..3], 80 * 12.5ns = 1uS, up/down (unint8,uint16,bool)
  timerAttachInterrupt(Read_timer, &onReadTimer, true);   //true=edge triggered
  timerAlarmWrite(Read_timer, 2000, true); //2000*100uS=200mS     //generate interrupt when counter reaches this value, number of uS per sample ( ,unit64,bool auto reload)
  timerAlarmEnable(Read_timer);                 //enable

  BufferFull=1;

//LED strands
  pixels.begin(); // Initialisierung der NeoPixel

  pixels.setBrightness(32);
//Network setup
  Serial.println("Start Ethernet");
  WiFi.onEvent(WiFiEvent);
  WiFi.setHostname(hostname.c_str()); //define hostname before begin  
  // ETH.setHostname(hostname.c_str()); //crashes ESP32
  //this may give eth debug Serial.setDebugOutput(true)
  ETH.begin();
  Serial.print("Mac Addr: ");Serial.println(ETH.macAddress()+" ");
  Serial.println("Setup complete:"); 
  DebugLevel=2; 
}


////----------- L O O P ------------------------------------------------------------------------------------------
  
void loop() {
 
    //Serial.printf("count %u times\n", QCount1.count);Serial.printf("count %u times\n", SamplesBufferIndex);
    delay(20);

    packetSize = Udp.parsePacket();
    if (packetSize) {
      if (DebugLevel==2){packetInfo();}
      ReadUDPPacket();
    }

    if ((BufferFull !=0) and (UDPSend==1)){streamUDP();}
    BufferFull=0;

    pixels.setBrightness(LBr);

    if (LCtr<LTimer){
      if (LOn=1){CalcColoursSine(LPos,1,LBeat);}
      LPos+=LDist;
      LBeat+=LBDist;
      pixels.show(); // Durchführen der Pixel-Ansteuerung
      LCtr+=0.05;    //Add 50 mS
    }
    else {
      if (LOn=1){CalcColoursSolid(0);}
      LOn=0; 
    }  
    
//CalcColoursSine(LPos,1,LBeat);
//LPos+=0;
//LBeat+=2;
//pixels.show(); // Durchführen der Pixel-Ansteuerung
delay (30); // Pause, die LEDs bleiben in dieser Zeit aus
}













void packetInfo( ) {
    Serial.print("Received packet of size ");
    Serial.println(packetSize);
    Serial.print("From ");
    IPAddress remoteIp = Udp.remoteIP();
    Serial.print(remoteIp);
    Serial.print(", port ");
    Serial.println(Udp.remotePort());
    // read the packet into packetBufffer
    int len = Udp.read(packetBuffer, 255);
    if (len > 0) {
      packetBuffer[len] = 0;
    }
    Serial.println("Contents:");
    Serial.println(packetBuffer);
    // send a reply, to the IP address and port that sent us the packet we received
    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    Udp.write(ReplyBuffer,10);
    Udp.endPacket(); 
}

String ReadUDPSubString(byte Num) {
  String result = "";
  
  uint16_t ii,jj=0;
  for (ii=0;ii<packetSize;ii+=1){
    if (packetBuffer[ii]==char(44)){jj=jj+1;}       //char(44)=","
    if (jj==Num && packetBuffer[ii]!=char(44)){result=result+packetBuffer[ii];}
  }
  return(result);
}

//Read the UDP packet and set whatever
void ReadUDPPacket() {
    String ss="";
    int len = Udp.read(packetBuffer, 255);
    if (len > 0) {
      packetBuffer[len] = 0;
    }
    
    if ( packetBuffer[0]==char(58) ){  //":"
      ss=ReadUDPSubString(1);
      Serial.print("Read UDP ");Serial.println(ss);
 
      
     if ( packetBuffer[1]==char(43) ){//:+,      Request data stream
         IOCIP=Udp.remoteIP();IOCPort=Udp.remotePort();UDPSend=1;
         if (DebugLevel!=0){Serial.print("Data Stream Requested ");Serial.print(IOCIP);Serial.print(":");Serial.print(IOCPort);Serial.println("");}
      }

     if ( packetBuffer[1]==char(45) ){//:-,      Stop data stream
         IOCIP=Udp.remoteIP();IOCPort=Udp.remotePort();UDPSend=0;
         if (DebugLevel!=0){Serial.print("Data Stream Stopped ");Serial.print(IOCIP);Serial.println("");}
      }

     if ( packetBuffer[1]==char(90) ){//:Z,1,  debug level
        if (DebugLevel!=0){Serial.print("New Debug level ");Serial.print(DebugLevel);Serial.println("");}
        DebugLevel=ss.toInt();
      }  

     if ( packetBuffer[1]==char(80) ){//:P  Period
        LPeriod=ss.toFloat();
        if (DebugLevel!=0){Serial.print("Pattern Period ");Serial.print(LPeriod);Serial.println("");}
      }

     if ( packetBuffer[1]==char(78) ){//:N  Num Leds
        LNum=ss.toInt();
        if (DebugLevel!=0){Serial.print("LNum ");Serial.print(LNum);Serial.println("");}
      }

     if ( packetBuffer[1]==char(99) ){//:c colour 1
        LCol1=ss.toInt();
        if (DebugLevel!=0){Serial.print("Colour 1 ");Serial.print(LCol1);Serial.println("");}
      }

     if ( packetBuffer[1]==char(67) ){//:C colour 2
        LCol2=ss.toInt();
        if (DebugLevel!=0){Serial.print("Colour 2 ");Serial.print(LCol2);Serial.println("");}
      }

     if ( packetBuffer[1]==char(71) ){//:G Go
        LOn=ss.toInt();LCtr=0;
        if (DebugLevel!=0){Serial.print("On Off ");Serial.print(LOn);Serial.println("");}
      }

     if ( packetBuffer[1]==char(83) ){//:S Step size
        LStep=ss.toFloat();
        if (DebugLevel!=0){Serial.print("Step ");Serial.print(LStep);Serial.println("");}
      }

     if ( packetBuffer[1]==char(68) ){//:D Dist per step
        LDist=ss.toFloat();
        if (DebugLevel!=0){Serial.print("SDist ");Serial.print(LDist);Serial.println("");}
      }

     if ( packetBuffer[1]==char(77) ){//:M Mode
        LMode=ss.toInt();
        if (DebugLevel!=0){Serial.print("Mode ");Serial.print(LMode);Serial.println("");}
      }

      if ( packetBuffer[1]==char(84) ){//:T Timer
        LTimer=ss.toInt();
        if (DebugLevel!=0){Serial.print("Timer ");Serial.print(LTimer);Serial.println("");}
      }     

     if ( packetBuffer[1]==char(66) ){//:B beat distance degrees?
        LBDist=ss.toFloat();
        if (DebugLevel!=0){Serial.print("Beat Dist ");Serial.print(LBDist);Serial.println("");}
      }

     if ( packetBuffer[1]==char(66) ){//:B beat distance degrees?
        LBr=ss.toInt();
        if (DebugLevel!=0){Serial.print("Brightness");Serial.print(LBr);Serial.println("");}
      }

       //void RunScope(unsigned int RSCtr)
    }
  
}



void SendUDP(){

   Udp.beginPacket(IOCIP, IOCPort);
   Udp.write(ReplyBuffer,8);
   Udp.endPacket();

   if (DebugLevel!=0){
      Serial.print("UDP Send: ");
      for (int i=0; i<8; i++){ 
        Serial.print((ReplyBuffer[i]));
      }
      Serial.println("");
   }//debug
}





void streamUDP() {   // :GAAAAABBBBBCCCCCDDDDDXXXX; - T=trigger/time  A..D hex of raw data X=counter  starts with : ends with ;
uint8_t HexAscList [16]={48,49,50,51,52,53,54,55,56,57,65,66,67,68,69,70};
//uint8_t  ReplyBuffer[1200] = "acknowledged";       // a string to send back
word     RBufPos =0;               // position/current size of buffer
word     i,j;
  i=BufferFull;
  //memcpy(ReplyBuffer,encBuf,8);  //copy the quad counter values..
/*
    Serial.print(BufferFull);Serial.print(">");
    Serial.print(i);Serial.print(">");
    Serial.print(QCount1.count);Serial.print(">");
    Serial.print(SamplesBuffer[i]);Serial.print(">");
    //SendUDP();
    Serial.print(ReplyBuffer[0]);Serial.print(":");
    Serial.print(ReplyBuffer[1]);Serial.print(":");
    Serial.print(ReplyBuffer[2]);Serial.print(":");
    Serial.println(ReplyBuffer[3]);      ....> works  1664>1664>134150704>134168072>8:62:255:7  low bit first

*/    
    BufferFull=0;
    SendUDP();
}
