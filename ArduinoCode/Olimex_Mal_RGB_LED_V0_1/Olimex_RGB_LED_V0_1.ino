//adapt quad counter code used with picomotors to control LED chains

//https://adafruit.github.io/Adafruit_NeoPixel/html/class_adafruit___neo_pixel.html
//https://funduino.de/nr-17-ws2812-neopixel

String hostname = "SL-LRGB4";


//#include <Adafruit_NeoPixel.h>
#include <FastLED.h>
#include <SimplexNoise.h>

#ifdef __AVR__
#include <avr/power.h>
#endif

#include <ArduinoOTA.h>  // For enabling over-the-air updates

#define PIN 16 // Hier wird angegeben, an welchem digitalen Pin die WS2812 LEDs bzw. NeoPixel angeschlossen sind
#define NUMPIXELS 300 // Hier wird die Anzahl der angeschlossenen WS2812 LEDs bzw. NeoPixel angegeben

//Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

// LED
#define NUM_LEDS 300
#define LED_TYPE    WS2812B
#define COLOR_ORDER GRB
#define BRIGHTNESS 32
#define BRIGHTNESS_MAX 225
#define UPDATES_PER_SECOND 60

uint8_t gHue = 0; 
int led_brightness_level = 64;
bool gReverseDirection = false;

int LPeriod =  60;    // P     >Period in LEDs                            
float LNum    = 200;    // N     >Number to calculate
byte  LCol1   =   0;    // c     >First colour (pos in array)
byte  LCol2   =   0;    // C     >2nd colour
byte  LOn     =   1;    // G
float LStep   =  1;   // S     >Seconds per change
float LDist   =    1;   // D     >Distance per change       -->added to LPos
float LBDist  =    1;   // B     >Beat Distance per change  -->added to LBeat  
byte  LMode   =    0;   // M     >0=Static  1=move
byte  LPerlin =    0;   // M     >0=Static  1=move
int32_t LTimer =  60;   // T     >seconds till off  (LOn set to 0) - compared to LCtr
int LBr = 32;           // I     >LED intensity/brightness 0..255

float LPos;             //       >Phase of the sine wave in LEDs
float LBeat;            //       >fading of one colour into another in degrees?
float LCtr;             //       >Time counting up in ~Seconds  
int stepper = 0;
int colord = 0;

// ----- COLORS ----------------------------------------------------
// Plain colors

CRGB colors[18] = {
  CRGB(  0,   0,   0),  //0 Black
  CRGB(170,   0,   0),  //1  dark red 
  CRGB(255,   0,   0),  //2  red
  CRGB(255, 170,   0),  //3  orange
  CRGB(255, 255,   0),  //4  yellow
  CRGB(85 , 255, 127),  //5  bright green 
  CRGB(  0, 170,   0),  //6  dark green
  CRGB(  0,  85,   0),  //7  darker green
  CRGB(  0, 170, 127),  //8  dirty green
  CRGB(  0,  85, 127),  //9  dirty blue
  CRGB(  0, 170, 255),  //10  sky blue
  CRGB(170, 255, 255),  //11 light blue
  CRGB(  0,   0, 255),  //12 royal blue
  CRGB(  0,   0, 127),  //13 dark blue
  CRGB(255,   0, 255),  //14 pink
  CRGB(170,   0, 255),  //15 purple
  CRGB(170,   0, 127),  //16 red/purple
  CRGB(200, 200, 200)  //17 White
};

// Color palettes
DEFINE_GRADIENT_PALETTE( my_candle ) {
    0,  255, 140, 0, // DarkOrange
   85,  255, 165, 0, // Orange
  170,  255,  69, 0, // OrangeRed
  255,  255,  69, 0
};  // "And then just duplicating it here"

DEFINE_GRADIENT_PALETTE( my_xmas ) {
    0, 179,   0,  12,
   85, 220,  61,  42,
  130,  13, 239,  66,
  170,   0, 179,  44,
  200,  13,  89,   1,
  255,  13,  89,   1
};  // "And then just duplicating it here"

DEFINE_GRADIENT_PALETTE( my_cyberpunk ) {
    0,   0, 255,  159,
    5,   0, 255,  159,
   65,   0, 184,  255,
  130,   0,  30,  255,
  185, 189,   0,  255,
  200, 214,   0,  255,
  255, 214,   0,  255
};  // "And then just duplicating it here"

CRGBPalette16 candy_cane;
/*
void setupStripedPalette( CRGB A, CRGB AB, CRGB B, CRGB BA)
{
  // Sets up a palette with alternating stripes of
  // colors "A" and "B" -- with color "AB" between 
  // where A fades into B, and color "BA" where B fades
  // into A.
  // The stripes of "A" are narrower than the stripes of "B",
  // but an equal-width arrangement is also shown.
  currentPalette = CRGBPalette16( 
        A, A, A, A, AB, B, B, B, B, B, B, B, B, B, B, BA
  //    A, A, A, A, A, A, A, AB, B, B, B, B, B, B, B, BA
  );
}
*/

/*
DEFINE_GRADIENT_PALETTE( custom_palette ) {
    0,  255, 140, 0, // DarkOrange
    1,  255,  69, 0, // OrangeRed
    2,  255,  69, 0
  };  // "And then just duplicating it here"
*/

CRGBPalette16 currentPalette;
CRGBPalette16 my_palette = my_xmas;
TBlendType    currentBlending;

// LEDS
CRGB leds[NUM_LEDS];
//CHSV hsv( random8(), 255, 255 );  //pick random Hue
//CHSV hsv( random8(), random8(), random8() );  //pick random HSV
CHSV hsv;  //pick random HSV
CHSV hsv2;
CRGB rgb;

// Noise
SimplexNoise sn;
#define NOISE_STEP_MICRO 1 // 0.7 /*original 0.01 noise multiplied by 1000ms*/
//#define NOISE_POS 0.1
#define NOISE_POS 0.05
#define EIGHT_BIT 255
double noise_simplex;
int noise_offset[NUM_LEDS];

// ----- UDP ------------------------------------

uint32_t  SamplesBuffer[2048];
uint32_t  SamplesBufferIndex;
uint32_t  BufferFull;   //0=2nd half full  1024=first half, else 1= not ready
int32_t    encBuf[2];
const word BufferSendSize  =256;  //power of 2=>128*4bytes*2channels=1024 bytes in packet....
const word BufferSendSizeM1=255;

hw_timer_t *Read_timer = NULL;

//Ethernet stuff
#define ETH_CLK_MODE ETH_CLOCK_GPIO17_OUT
#define ETH_PHY_POWER 12

#include <ETH.h>           //ETH.h uses the RJ45 rather than the actual wifi
#include <WiFiUdp.h>
static bool eth_connected = false;
char packetBuffer[255]; //buffer to hold incoming packet
uint8_t  ReplyBuffer[1200] = "acknowledged";       // a string to send back
uint8_t  ReplyBuffer2[1000] = "acknowledged"; 


int packetSize;
byte UDPSend=0;
  WiFiUDP Udp;

IPAddress IOCIP,IOCIP2;
unsigned int IOCPort = 0;

//General
byte DebugLevel;

void IRAM_ATTR onReadTimer(){     //Sample timer - store position when ticks
  BufferFull=1;
}

void setup() {

  byte i;

  // Serial setup
  Serial.begin(115200);

  //Network setup
  Serial.println("Start Ethernet");
  WiFi.onEvent(WiFiEvent);
  WiFi.setHostname(hostname.c_str()); //define hostname before begin  
  // ETH.setHostname(hostname.c_str()); //crashes ESP32
  //this may give eth debug Serial.setDebugOutput(true)
  ETH.begin();
  Serial.print("Mac Addr: ");Serial.println(ETH.macAddress()+" ");
  Serial.println("Setup complete:"); 
  DebugLevel=2;

  // timer setup
  Read_timer = timerBegin(0, 8000, true); // 100uS       //timer number [0..3], 80 * 12.5ns = 1uS, up/down (unint8,uint16,bool)
  timerAttachInterrupt(Read_timer, &onReadTimer, true);   //true=edge triggered
  timerAlarmWrite(Read_timer, 2000, true); //2000*100uS=200mS     //generate interrupt when counter reaches this value, number of uS per sample ( ,unit64,bool auto reload)
  timerAlarmEnable(Read_timer);                 //enable

  BufferFull=1;

  // LED setup
  currentPalette = my_palette;            // TODO change this from code
  currentBlending = LINEARBLEND;          // TODO change this from code
  NoiseIndexShift();

  LEDS.addLeds<LED_TYPE, PIN, COLOR_ORDER>(leds, NUM_LEDS);
  FastLED.setBrightness(BRIGHTNESS);
  FastLED.clear();

  // OTA ---------
  ArduinoOTA.begin();  // Starts OTA
}

////----------- L O O P ------------------------------------------------------------------------------------------
  
void loop() {
 
    //Serial.printf("count %u times\n", QCount1.count);Serial.printf("count %u times\n", SamplesBufferIndex);
    delay(5);

    // Handle Arduino OTA
    ArduinoOTA.handle();

    // Receive udp packets
    packetSize = Udp.parsePacket();
    if (packetSize) {
      if (DebugLevel==2){packetInfo();}
      ReadUDPPacket();
    }

    // stream UDP
    if ((BufferFull !=0) and (UDPSend==1)){streamUDP();}
    BufferFull=0;

    //UPDATES_PER_SECOND = LPeriod;

    // LED MODES

    if ( LMode==0 ){ // single color Table mode
        //if (DebugLevel!=0){Serial.print("Color RGB ");Serial.print(colors[LCol1].r);Serial.println("");}
        //if (DebugLevel!=0){Serial.print("Color RGB ");Serial.print(colors[LCol1].g);Serial.println("");}
        //if (DebugLevel!=0){Serial.print("Color RGB ");Serial.print(colors[LCol1].b);Serial.println("");}
        hsv = rgb2hsv_approximate( colors[LCol1] );
        //if (DebugLevel!=0){Serial.print("Color HVS H ");Serial.print(hsv.h);Serial.println("");}
        //if (DebugLevel!=0){Serial.print("Color HVS S ");Serial.print(hsv.s);Serial.println("");}
        //if (DebugLevel!=0){Serial.print("Color HVS V ");Serial.print(hsv.v);Serial.println("");}
        fill_solid(leds, NUM_LEDS, hsv ); // CHSV (hue, saturation, value);
        FastLED.show();
        }

    if ( LMode==1 ){ // single color Table mode
        //CRGBPalette16 my_palette = my_cyberpunk;
        leds_perlin(my_cyberpunk);
        //leds_perlin(LPerlin); // TODO implement different gradients
        }

    if ( LMode==2 ){ // single color Table mode
        //CRGBPalette16 my_palette = my_cyberpunk;
        //leds_event(CRGB::Red, CRGB::Red, CRGB::Green, CRGB::Green);
        leds_event(colors[LCol1], colors[LCol1], colors[LCol2], colors[LCol2]);
        }

    if ( LMode==3 ){ // sine wave
      //if (DebugLevel!=0){Serial.print("Mode ");Serial.print(LMode);Serial.println("");}
        int pos = beatsin16(5,0,192); // generating the sinwave 
        fill_solid(leds, NUM_LEDS, CHSV( gHue, 255, pos)); // CHSV (hue, saturation, value);
        FastLED.show();
        EVERY_N_MILLISECONDS(100) {gHue++;} 
    }

    if ( LMode==5 ){ // table dual
        leds_dual(colors[LCol1], colors[LCol2]);
    }
    
  //delay( 1000 / UPDATES_PER_SECOND ); // Pause, die LEDs bleiben in dieser Zeit aus
  //FastLED.delay(1000 / UPDATES_PER_SECOND);
  FastLED.delay(1000 / LPeriod);
}

// FUNCTIONS ################################################################

void WiFiEvent(WiFiEvent_t event)
{
  Serial.println("ETH Event....");
  Serial.println(ETH.getHostname());
  switch (event) {
    case SYSTEM_EVENT_ETH_START:
      Serial.println("ETH Started");
      //set eth hostname here
      ETH.setHostname(hostname.c_str());
      break;
    case SYSTEM_EVENT_ETH_CONNECTED:
      Serial.println("ETH Connected");
      break;
    case SYSTEM_EVENT_ETH_GOT_IP:
      Serial.print("ETH MAC: ");
      Serial.print(ETH.macAddress());
      Serial.print(", IPv4: ");
      Serial.print(ETH.localIP());
      //DispScrollString (ETH.localIP());// is an array so need to be cleverer
      if (ETH.fullDuplex()) {
        Serial.print(", FULL_DUPLEX");
      }
      Serial.print(", ");
      Serial.print(ETH.linkSpeed());
      Serial.println("Mbps");
      eth_connected = true;
      Udp.begin(44444);
      break;
    case SYSTEM_EVENT_ETH_DISCONNECTED:
      Serial.println("ETH Disconnected");
      eth_connected = false;
      break;
    case SYSTEM_EVENT_ETH_STOP:
      Serial.println("ETH Stopped");
      eth_connected = false;
      break;
    default:
      break;
  }
}

void packetInfo( ) {
    Serial.print("Received packet of size ");
    Serial.println(packetSize);
    Serial.print("From ");
    IPAddress remoteIp = Udp.remoteIP();
    Serial.print(remoteIp);
    Serial.print(", port ");
    Serial.println(Udp.remotePort());
    // read the packet into packetBufffer
    int len = Udp.read(packetBuffer, 255);
    if (len > 0) {
      packetBuffer[len] = 0;
    }
    Serial.println("Contents:");
    Serial.println(packetBuffer);
    // send a reply, to the IP address and port that sent us the packet we received
    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    Udp.write(ReplyBuffer,10);
    Udp.endPacket(); 
}

String ReadUDPSubString(byte Num) {
  String result = "";
  
  uint16_t ii,jj=0;
  for (ii=0;ii<packetSize;ii+=1){
    if (packetBuffer[ii]==char(44)){jj=jj+1;}       //char(44)=","
    if (jj==Num && packetBuffer[ii]!=char(44)){result=result+packetBuffer[ii];}
  }
  return(result);
}

//Read the UDP packet and set whatever
void ReadUDPPacket() {
    String ss="";
    int len = Udp.read(packetBuffer, 255);
    if (len > 0) {
      packetBuffer[len] = 0;
    }
    
    if ( packetBuffer[0]==char(58) ){  //":"
      ss=ReadUDPSubString(1);
      Serial.print("Read UDP ");Serial.println(ss);
 
      
     if ( packetBuffer[1]==char(43) ){//:+,      Request data stream
         IOCIP=Udp.remoteIP();IOCPort=Udp.remotePort();UDPSend=1;
         if (DebugLevel!=0){Serial.print("Data Stream Requested ");Serial.print(IOCIP);Serial.print(":");Serial.print(IOCPort);Serial.println("");}
      }

     if ( packetBuffer[1]==char(45) ){//:-,      Stop data stream
         IOCIP=Udp.remoteIP();IOCPort=Udp.remotePort();UDPSend=0;
         if (DebugLevel!=0){Serial.print("Data Stream Stopped ");Serial.print(IOCIP);Serial.println("");}
      }

     if ( packetBuffer[1]==char(90) ){//:Z,1,  debug level
        if (DebugLevel!=0){Serial.print("New Debug level ");Serial.print(DebugLevel);Serial.println("");}
        DebugLevel=ss.toInt();
      }  

     if ( packetBuffer[1]==char(80) ){//:P  Period
        LPeriod=ss.toInt();
        if ( LPeriod < 30){ LPeriod = 30;}
        if (DebugLevel!=0){Serial.print("Pattern Period ");Serial.print(LPeriod);Serial.println("");}
      }

     if ( packetBuffer[1]==char(78) ){//:N  Num Leds
        LNum=ss.toInt();
        if (DebugLevel!=0){Serial.print("LNum ");Serial.print(LNum);Serial.println("");}
        FastLED.clear();
      }

     if ( packetBuffer[1]==char(99) ){//:c colour 1
        LCol1=ss.toInt();
        if (DebugLevel!=0){Serial.print("Colour 1 ");Serial.print(LCol1);Serial.println("");}
        // Color stripes with NO gaps between them -- colors will crossfade
        //setupStripedPalette( CRGB::Red, CRGB::Red, CRGB::Green, CRGB::Green);

      }

     if ( packetBuffer[1]==char(67) ){//:C colour 2
        LCol2=ss.toInt();
        if (DebugLevel!=0){Serial.print("Colour 2 ");Serial.print(LCol2);Serial.println("");}
        // Color stripes with NO gaps between them -- colors will crossfade
        //setupStripedPalette( CRGB::Red, CRGB::Red, CRGB::Green, CRGB::Green);

      }

     if ( packetBuffer[1]==char(71) ){//:G Go
        LOn=ss.toInt();LCtr=0;
        if (DebugLevel!=0){Serial.print("On Off ");Serial.print(LOn);Serial.println("");}
      }

     if ( packetBuffer[1]==char(83) ){//:S Step size
        LStep=ss.toFloat();
        if (DebugLevel!=0){Serial.print("Step ");Serial.print(LStep);Serial.println("");}
      }

     if ( packetBuffer[1]==char(68) ){//:D Dist per step
        LDist=ss.toFloat();
        if (DebugLevel!=0){Serial.print("SDist ");Serial.print(LDist);Serial.println("");}
      }

     if ( packetBuffer[1]==char(77) ){//:M Mode
        LMode=ss.toInt();
        if (DebugLevel!=0){Serial.print("Mode ");Serial.print(LMode);Serial.println("");}
      }

      if ( packetBuffer[1]==char(84) ){//:T Timer
        LTimer=ss.toInt();
        if (DebugLevel!=0){Serial.print("Timer ");Serial.print(LTimer);Serial.println("");}
      }     

     if ( packetBuffer[1]==char(66) ){//:B beat distance degrees?
        LBDist=ss.toFloat();
        if (DebugLevel!=0){Serial.print("Beat Dist ");Serial.print(LBDist);Serial.println("");}
      }

     if ( packetBuffer[1]==char(98) ){//:b brightness
        LBr=ss.toInt();
        if (DebugLevel!=0){Serial.print("Brightness ");Serial.print(LBr);Serial.println("");}
        FastLED.setBrightness(LBr);
        FastLED.show();
      }

      if ( packetBuffer[1]==char(73) ){//:I brightness/Intensity
        LBr=ss.toInt();
        if (DebugLevel!=0){Serial.print("Brightness ");Serial.print(LBr);Serial.println("");}
        FastLED.setBrightness(LBr);
        FastLED.show();
      }

       //void RunScope(unsigned int RSCtr)
    }
  
}

void SendUDP(){

   Udp.beginPacket(IOCIP, IOCPort);
   Udp.write(ReplyBuffer,8);
   Udp.endPacket();

   if (DebugLevel!=0){
      Serial.print("UDP Send: ");
      for (int i=0; i<8; i++){ 
        Serial.print((ReplyBuffer[i]));
      }
      Serial.println("");
   }//debug
}

void streamUDP() {   // :GAAAAABBBBBCCCCCDDDDDXXXX; - T=trigger/time  A..D hex of raw data X=counter  starts with : ends with ;
uint8_t HexAscList [16]={48,49,50,51,52,53,54,55,56,57,65,66,67,68,69,70};
//uint8_t  ReplyBuffer[1200] = "acknowledged";       // a string to send back
word     RBufPos =0;               // position/current size of buffer
word     i,j;
  i=BufferFull;

    BufferFull=0;
    SendUDP();
}


// ---------- LED FUNCTIONS ----------------------------
void NoiseIndexShift()
{
  // Could be improved by calling in each loop an iterating sn.noise with time

    for ( int i = 0; i < NUM_LEDS; i++) {
      noise_simplex = sn.noise(0, i * NOISE_POS);
      float color_temp = map(noise_simplex*100, -100, 100, 0, EIGHT_BIT);
      noise_offset[i] = color_temp;
    } 
}

void FillLEDsFromPaletteColors( uint8_t colorIndex)
{
  uint8_t brightness = led_brightness_level;

  for ( int i = 0; i < NUM_LEDS; i++) {
    leds[i] = ColorFromPalette( currentPalette, colorIndex + noise_offset[i], brightness, currentBlending);
    //uint8_t heatindex = (something from 0-255);
    //leds[i] = ColorFromPalette( my_palette, heatindex); // normal palette access
    colorIndex += 1;
  }
}

void leds_mono()
{
  //if (DebugLevel!=0){Serial.print("Color RGB ");Serial.print(colors[LCol1].r);Serial.println("");}
  //if (DebugLevel!=0){Serial.print("Color RGB ");Serial.print(colors[LCol1].g);Serial.println("");}
  //if (DebugLevel!=0){Serial.print("Color RGB ");Serial.print(colors[LCol1].b);Serial.println("");}
  hsv = rgb2hsv_approximate( colors[LCol1] );
  //if (DebugLevel!=0){Serial.print("Color HVS H ");Serial.print(hsv.h);Serial.println("");}
  //if (DebugLevel!=0){Serial.print("Color HVS S ");Serial.print(hsv.s);Serial.println("");}
  //if (DebugLevel!=0){Serial.print("Color HVS V ");Serial.print(hsv.v);Serial.println("");}
  fill_solid(leds, NUM_LEDS, hsv ); // CHSV (hue, saturation, value);
  FastLED.show();
  //FastLED.delay(1000 / UPDATES_PER_SECOND);
}

void leds_dual(CRGB color1, CRGB color2)
{
  //hsv = rgb2hsv_approximate( color1 );
  //hsv2 = rgb2hsv_approximate( color2 );

  CRGBPalette16 custom_palette = CRGBPalette16(
                                   color1,  color1,  color2,  color1,
                                   color2, color1, color1,  color1,
                                   color2,  color1,  color2,  color1,
                                   color2, color2, color1,  color2 );

  currentPalette = custom_palette;

  static uint8_t startIndex = 0;
  startIndex = startIndex + LStep; /* motion speed */

  FillLEDsFromPaletteColors(startIndex);
  FastLED.show();
  //FastLED.delay(1000 / UPDATES_PER_SECOND);
}

void leds_perlin( CRGBPalette16 palette)
{
  currentPalette = palette;

  static uint8_t startIndex = 0;
  startIndex = startIndex + LStep; /* motion speed */

  FillLEDsFromPaletteColors(startIndex);
  FastLED.show();
  //FastLED.delay(1000 / UPDATES_PER_SECOND);
}

void leds_event(CRGB A, CRGB AB, CRGB B, CRGB BA)
{

  candy_cane = CRGBPalette16( 
        A, A, A, A, AB, B, B, B, B, B, B, B, B, B, B, BA
  //    A, A, A, A, A, A, A, AB, B, B, B, B, B, B, B, BA
  );

  static uint8_t startIndex = 0;
  startIndex = startIndex + 2 * LStep; /* higher = faster motion */

  fill_palette( leds, NUM_LEDS, 
                startIndex, 8, /* higher = narrower stripes */ 
                candy_cane, 255, LINEARBLEND);

  FastLED.show();
  //FastLED.delay(1000 / UPDATES_PER_SECOND);
}

void leds_sin()
{
  //if (DebugLevel!=0){Serial.print("Mode ");Serial.print(LMode);Serial.println("");}
  int pos = beatsin16(5,0,192); // generating the sinwave 
  fill_solid(leds, NUM_LEDS, CHSV( gHue, 255, pos)); // CHSV (hue, saturation, value);
  FastLED.show();
  
  EVERY_N_MILLISECONDS(100) {gHue++;}
}

void leds_beacon()
{
  static uint8_t startIndex = 0;
  startIndex = startIndex + 1; /* motion speed */

  FillLEDsFromPaletteColors(startIndex);
  FastLED.show();
  //FastLED.delay(1000 / UPDATES_PER_SECOND);

  /*
  CRGBArray<NUM_LEDS> leds;
  CRGB beginning [10];
  CRGB middle [10];
  CRGB end [10];

  beginning = leds(1,10);
  middle = leds(11,20);
  end = leds(21,30);

  beginning = CRGB::Black;
  middle = CRGB::Red;
  end = CRGB:: Black;
  */
}