#include <FastLED.h>
#include <SimplexNoise.h>

// mode selector
#define MODE_PIN 7 // 7 - D7
byte mode_selector;
bool next_state_0;
bool next_state_2;

/////////////////////////////////////////////
// LED VARIABLES
#define LED_PIN     16 // 6 - D6
#define COLOR_ORDER GRB /*GRB*/
#define CHIPSET     WS2812B
#define NUM_LEDS    299
#define UPDATES_PER_SECOND 120
#define BRIGHTNESS 64
#define led_brightness_level_max 225
int led_brightness_level = 64;
bool gReverseDirection = false;

DEFINE_GRADIENT_PALETTE( my_gradient ) {
  0,    255,140,0, // DarkOrange
 85,    255,165,0, // Orange
170,    255,69,0, // OrangeRed
255,    255,69,0
};  // "And then just duplicating it here"

DEFINE_GRADIENT_PALETTE( my_xmas ) {
  0,    179,0,12,
 85,    220,61,42,
130,    13,239,66,
170,    0,179,44,
200,    13,89,1,
255,    13,89,1
};  // "And then just duplicating it here"

DEFINE_GRADIENT_PALETTE( my_cyberpunk ) {
  0,    0,255,159,
  5,    0,255,159,
 65,    0,184,255,
130,    0,30,255,
185,    189,0,255,
200,    214,0,255,
255,    214,0,255
};  // "And then just duplicating it here"

CRGBPalette16 currentPalette;
//CRGBPalette16 my_palette = CRGBPalette16(CRGB::DarkOrange,CRGB::Orange, CRGB::OrangeRed);
CRGBPalette16 my_palette = my_xmas;
TBlendType    currentBlending;
CRGB leds[NUM_LEDS];

SimplexNoise sn;
#define NOISE_STEP_MICRO 1 // 0.7 /*original 0.01 noise multiplied by 1000ms*/
//#define NOISE_POS 0.1
#define NOISE_POS 0.05
#define EIGHT_BIT 255
double noise_simplex;
int offset[NUM_LEDS];

/////////////////////////////////////////////
// OTHER VARIABLES
int stepsize;
int wavesize;
float wavewidth;
unsigned long startTime;
//float switchOffTime = 3.6e+6;
unsigned long switchOffTime = 5e5;

void setup() {
  delay(3000); // sanity delay

  // initialize Serial output for debug
  Serial.begin(9600);
  
  // initialize pins
  pinMode(LED_PIN, OUTPUT);
  
  // set mode selector
  // mode 0 offline
  // mode 1 led lights on
  // mode 2 fire on
  mode_selector = 0;
  next_state_0 = true;
  
  // initialize LED matrix
  FastLED.addLeds<CHIPSET, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
  FastLED.setBrightness(BRIGHTNESS);

  //currentPalette = LavaColors_p;
  currentPalette = my_palette;
  currentBlending = LINEARBLEND;
  colorIndexShift();
  // initialize timers
  //startTime = millis();
}

void loop() {

  // loop clear
  //FastLED.clear(true);

  set_led();
  info();
}

void shoud_we_reset_timer(int m)
{
  unsigned long elapsed_time = millis() - startTime;
  //Serial.print(elapsed_time);
  //Serial.print("\n");
  if (elapsed_time > switchOffTime * long(m)){
        mode_selector = 0;
        Serial.print("Revert to mode 0");
  } 
}

void info()
{
  Serial.print("System in mode: ");
  Serial.print(mode_selector);
  Serial.print("\n");
  
  Serial.print("Brightness ");
  Serial.print(BRIGHTNESS);
  Serial.print("\n");
}

//// LED functions
void FillLEDsFromPaletteColors( uint8_t colorIndex)
{
  uint8_t brightness = led_brightness_level;

  for ( int i = 0; i < NUM_LEDS; i++) {
    leds[i] = ColorFromPalette( currentPalette, colorIndex + offset[i], brightness, currentBlending);
    //uint8_t heatindex = (something from 0-255);
    //leds[i] = ColorFromPalette( my_palette, heatindex); // normal palette access
    colorIndex += 1;
  }
}

void colorIndexShift()
{
    Serial.print("Generating offset. \n");
    for ( int i = 0; i < NUM_LEDS; i++) {
      noise_simplex = sn.noise(0, i * NOISE_POS);
      float color_temp = map(noise_simplex*100, -100, 100, 0, EIGHT_BIT);
      offset[i] = color_temp;
      Serial.print(offset[i]);
      Serial.print("\n");
    }
    Serial.print("Offset generated. \n");
    
}

void set_led()
{
  static uint8_t startIndex = 0;
  startIndex = startIndex + 1; /* motion speed */

  FillLEDsFromPaletteColors(startIndex);
  FastLED.show();
  FastLED.delay(1000 / UPDATES_PER_SECOND);
}
