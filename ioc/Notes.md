# Basic functions

[] Set number of LEDs
[] Set color
[] On/off button
[] Set brightness
[] Set timer

# Extended functions

[] Set table mode
[] Set two colors
[] Set transition
[] Set noise offset

# Extra functions

[] Global turn on/off
[] Global timer
[] Calculate side LEDS
[] Set Beamer mode
[] Set beamer speed
[] 