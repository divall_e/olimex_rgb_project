#!/bin/bash

## copy script to sf_maloja_caqtdm

XLIGHT_SYS="SATES20-XLIGHT"

BR_MIN=0
BR_MID=100
BR_MAX=220


#/*for i in $(eval echo "{$START..$END}")*/


################################# SETUP

# Brightness to 0
for i in {1..4}
do
   #echo "caget ${XLIGHT_SYS}${i}:BRIGHTNESS"
   caput "${XLIGHT_SYS}${i}:BRIGHTNESS" "0"
done


# Mode to 0 =  solid color
for i in {1..4}
do
   caput "${XLIGHT_SYS}${i}:MODE" "0"
done

sleep 0.05

# set speed slow
  for i in {1..4}
  do
   caput "${XLIGHT_SYS}${i}:SIZE" "1"
   sleep 0.1
  done

sleep 0.05

# start LED loop
for i in {1..4}
do
   #echo "caget ${XLIGHT_SYS}${i}:BRIGHTNESS"
   cagets "${XLIGHT_SYS}${i}:START"
done

sleep 0.05

# Color1 to RED=1..2
for i in {1..4}
do
   caput "${XLIGHT_SYS}${i}:COLOR1" "2"
done

sleep 0.05

# Color2 to 0=OFF
for i in {1..4}
do
   caput "${XLIGHT_SYS}${i}:COLOR2" "0"
done

#sleep(50)

sleep 1

###########################################3 BLINK ONE

# flash a single table
for br in {1..220..40}
do
   #echo "caput ${XLIGHT_SYS}1:BRIGHTNESS ${br}"
   caput "${XLIGHT_SYS}1:BRIGHTNESS" "${br}"
   #sleep 0.05
done

for br in {240..0..40}
do
   caput "${XLIGHT_SYS}1:BRIGHTNESS" "${br}"
   #sleep 0.05
done

sleep 4

###################################### BLINK ONE SLOW

# flash a single table slower
for br in {1..220..20}
do
   caput "${XLIGHT_SYS}1:BRIGHTNESS" "${br}"
   sleep 0.05
done

for br in {240..0..40}
do
   caput "${XLIGHT_SYS}1:BRIGHTNESS" "${br}"
   sleep 0.05
done

sleep 1

###################################### FLASH ALL 

# flash all tables
for br in {0..250..50}
do
   for i in {1..4}
   do
      caput "${XLIGHT_SYS}${i}:BRIGHTNESS" "${br}"
      sleep 0.05
   done
done

for br in {250..0..50}
do
   for i in {1..4}
   do
      caput "${XLIGHT_SYS}${i}:BRIGHTNESS" "${br}"
      sleep 0.05
   done
done

sleep 0.5

######################################## FLASH IN SEQ

# flash in seqenece
for br in {0..400..25}
do
  for i in {1..4}
  do
    local_br=$(( br - 50 ))
    #echo ${local_br}
    caput "${XLIGHT_SYS}${i}:BRIGHTNESS" "${local_br}"
    sleep 0.05
  done
done

# flash in seqenece down
for br in {400..0..25}
do
  for i in {1..4}
  do
    local_br=$(( br - 50 ))
    #echo ${local_br}
    caput "${XLIGHT_SYS}${i}:BRIGHTNESS" "${local_br}"
    sleep 0.05
  done
done

# Brightness to 0
for i in {1..4}
do
   #echo "caget ${XLIGHT_SYS}${i}:BRIGHTNESS"
   caput "${XLIGHT_SYS}${i}:BRIGHTNESS" "0"
done

##################################### FLASH IN SEQ SLOW

# flash in seqenece slower
for br in {0..400..25}
do
  for i in {1..4}
  do
    local_br=${ br - 50 }
    echo ${local_br}
    caput "${XLIGHT_SYS}${i}:BRIGHTNESS" "${local_br}"
    sleep 0.7
  done
done

# flash in seqenece slower
for br in {400..0..25}
do
  for i in {1..4}
  do
    local_br=${ br - 50 }
    echo ${local_br}
    caput "${XLIGHT_SYS}${i}:BRIGHTNESS" "${local_br}"
    sleep 0.1
  done
done

# Brightness to 0
for i in {1..4}
do
   #echo "caget ${XLIGHT_SYS}${i}:BRIGHTNESS"
   caput "${XLIGHT_SYS}${i}:BRIGHTNESS" "0"
done

####################################### CANDY CANE
echo "candy cane red white"

# Mode to 2 = event
for i in {1..4}
do
   caput "${XLIGHT_SYS}${i}:MODE" "2"
done

# Color1 to RED=1..2
for i in {1..4}
do
   caput "${XLIGHT_SYS}${i}:COLOR1" "2"
done

# Color2 to 0=OFF
for i in {1..4}
do
   caput "${XLIGHT_SYS}${i}:COLOR2" "0"
done

# flash all tables
for br in {1..150..15}
do
  for i in {1..4}
  do
   caput "${XLIGHT_SYS}${i}:BRIGHTNESS" "${br}"
   #sleep 0.05
  done
done

sleep 5

for br in {150..30..15}
do
  for i in {1..4}
  do
   caput "${XLIGHT_SYS}${i}:BRIGHTNESS" "${br}"
   sleep 0.05
  done
done

########################## CANDY CANE RED GREEN
echo "candy cane red green"

# Mode to 2 = event
for i in {1..4}
do
   caput "${XLIGHT_SYS}${i}:MODE" "2"
done

# Color1 to RED=1..2
for i in {1..4}
do
   caput "${XLIGHT_SYS}${i}:COLOR1" "2"
done

# Color2 to 0=OFF
for i in {1..4}
do
   caput "${XLIGHT_SYS}${i}:COLOR2" "6"
done

# flash all tables
for br in {30..175..15}
do
  for i in {1..4}
  do
   caput "${XLIGHT_SYS}${i}:BRIGHTNESS" "${br}"
   sleep 0.05
  done
done

sleep 6

############################## FASTER

# set speed faster
for speed in {1..12..2}
do
  for i in {1..4}
  do
   caput "${XLIGHT_SYS}${i}:SIZE" "${speed}"
   sleep 0.1
  done
done

sleep 5

############################# SET GRADIENT
echo "set gradient"

# Mode to 5 = tableGR
for i in {1..4}
do
   caput "${XLIGHT_SYS}${i}:MODE" "1"
done

sleep 10

for i in {1..4}
  do
   caput "${XLIGHT_SYS}${i}:SIZE" "6"
   sleep 0.1
  done

############################# SET TWINKLE
echo "set twinkle"

# Mode to 5 = tableGR
for i in {1..4}
do
   caput "${XLIGHT_SYS}${i}:MODE" "1"
done

# Color1 to RED=1..2
for i in {1..4}
do
   caput "${XLIGHT_SYS}${i}:COLOR1" "2"
done

# Color2 to 0=OFF
for i in {1..4}
do
   caput "${XLIGHT_SYS}${i}:COLOR2" "4"
done

sleep 10

############################

# set speed slower
for speed in {20..4..2}
do
  for i in {1..4}
  do
   caput "${XLIGHT_SYS}${i}:SIZE" "${speed}"
   sleep 0.1
  done
done

sleep 5

######################## SET TABLE
#### clear

# Brightness to 0
for i in {1..4}
do
   #echo "caget ${XLIGHT_SYS}${i}:BRIGHTNESS"
   caput "${XLIGHT_SYS}${i}:BRIGHTNESS" "0"
done


# Mode to 0 =  solid color
for i in {1..4}
do
   caput "${XLIGHT_SYS}${i}:MODE" "0"
done

sleep 0.05

# set speed slow
  for i in {1..4}
  do
   caput "${XLIGHT_SYS}${i}:size" "1"
   sleep 0.1
  done

sleep 0.05

# Color1 to RED=1..2
for i in {1..4}
do
   caput "${XLIGHT_SYS}${i}:COLOR1" "2"
done

sleep 0.05

# Color2 to 0=OFF
for i in {1..4}
do
   caput "${XLIGHT_SYS}${i}:COLOR2" "0"
done

#### mode
# Mode to 0 = table
for i in {1..4}
do
   caput "${XLIGHT_SYS}${i}:MODE" "0"
done

sleep 1

# flash a single table slower
# Color1 to RED=1..2
caput "${XLIGHT_SYS}1:COLOR1" "2"

for br in {1..220..20}
do
   caput "${XLIGHT_SYS}1:BRIGHTNESS" "${br}"
   sleep 0.05
done

sleep 1.1

# flash a single table slower
# Color1 to RED=1..2
caput "${XLIGHT_SYS}2:COLOR1" "4"

for br in {1..220..20}
do
   caput "${XLIGHT_SYS}2:BRIGHTNESS" "${br}"
   sleep 0.05
done

sleep 1.1

# flash a single table slower
# Color1 to RED=1..2
caput "${XLIGHT_SYS}3:COLOR1" "7"

for br in {1..220..20}
do
   caput "${XLIGHT_SYS}3:BRIGHTNESS" "${br}"
   sleep 0.05
done

sleep 1.1

# flash a single table slower
# Color1 to RED=1..2
caput "${XLIGHT_SYS}4:COLOR1" "12"

for br in {1..220..20}
do
   caput "${XLIGHT_SYS}4:BRIGHTNESS" "${br}"
   sleep 0.05
done

